<?php require_once "./code.php" ?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>S01: PHP Basics and Selection Control Structures</title>
</head>

<body>
    <!-- Activity 1 -->
    <h1>Full Address</h1>
    <p><?= getFullAddress('Timog Avenue', 'Quezon City', 'Metro Manila', 'Philippines'); ?></p>
    <p><?= getFullAddress('Buendia Avenue', 'Makati City', 'Metro Manila', 'Philippines'); ?></p>

    <!-- Activity 2 -->
    <h1>Letter-Based Grading</h1>
    <p><?= getLetterGrade(87) ?></p>
    <p><?= getLetterGrade(94) ?></p>
    <p><?= getLetterGrade(74) ?></p>
</body>

</html>